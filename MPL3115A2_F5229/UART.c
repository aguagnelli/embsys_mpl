/* File:  UART.c
 * Author: Martin Dida
 * Created on: 11.11.2014
 * Brief: C source file for UART functionality
 */

/****************************** Includes ***********************************/
#include "UART.h"


/****************************** Definitions and Macros *******************************/


/****************************** Global variables *************************************/


/****************************** Function definitions *********************************/
void UART_init(uint8_t mode, uint8_t w_length, uint8_t parity, uint8_t even_odd, uint8_t stop_bit)
{
	UCA0CTL1 |= UCSWRST;			// Enable software reset of USCI module

	UCA0CTL1 |= UCSSEL0;			// Set ACLK clock source for USCI module
	UCA0CTL0 &= ~UCSYNC;			// Set asynchronous mode
	UCA0CTL0 &= ~UCMSB;				// Transfer LSB first

	if (mode == UART) {
		UCA0CTL0 &= UCMODE_0; 		// Set USCI module to UART mode
		if (w_length == EIGHT)
			UCA0CTL0 &= ~UC7BIT;	// Set 8bit long transfer frame
		if (w_length == SEVEN)
			UCA0CTL0 |= UC7BIT;		// Set 7bit long transfer frame
		if (parity == NOPARITY)
			UCA0CTL0 &= ~UCPEN;		// Disable parity bit
		if (parity == PARITY)
			UCA0CTL0 |= UCPEN; 		// Enable parity bit
		if (even_odd == ODD)
			UCA0CTL0 &= ~UCPAR;		// Set odd parity
		if (even_odd == EVEN)
			UCA0CTL0 |= UCPAR;		// Set even parity
		if (stop_bit == ONE_STOP)
			UCA0CTL0 &= ~UCSPB;		// Set one stop bit
		if (stop_bit == TWO_STOP)
			UCA0CTL0 |= UCSPB;		// Set two stop bits
	}

	if (mode == IDLE_LINE) {
		UCA0CTL0 |= UCMODE0;		// Set USCI module to Idle-line multiprocessor
		if (w_length == EIGHT)
			UCA0CTL0 &= ~UC7BIT;	// Set 8bit long transfer frame
		if (w_length == SEVEN)
			UCA0CTL0 |= UC7BIT;		// Set 7bit long transfer frame
		if (parity == NOPARITY)
			UCA0CTL0 &= ~UCPEN;		// Disable parity bit
		if (parity == PARITY)
			UCA0CTL0 |= UCPEN; 		// Enable parity bit
		if (even_odd == ODD)
			UCA0CTL0 &= ~UCPAR;		// Set odd parity
		if (even_odd == EVEN)
			UCA0CTL0 |= UCPAR;		// Set even parity
		if (stop_bit == ONE_STOP)
			UCA0CTL0 &= ~UCSPB;		// Set one stop bit
		if (stop_bit == TWO_STOP)
			UCA0CTL0 |= UCSPB;		// Set two stop bits
	}

	if (mode == MULTIPROC) {
		UCA0CTL0 |= UCMODE1;		// Set USCI module to Address multiprocessor mode
		if (w_length == EIGHT)
			UCA0CTL0 &= ~UC7BIT;	// Set 8bit long transfer frame
		if (w_length == SEVEN)
			UCA0CTL0 |= UC7BIT;		// Set 7bit long transfer frame
		if (parity == NOPARITY)
			UCA0CTL0 &= ~UCPEN;		// Disable parity bit
		if (parity == PARITY)
			UCA0CTL0 |= UCPEN; 		// Enable parity bit
		if (even_odd == ODD)
			UCA0CTL0 &= ~UCPAR;		// Set odd parity
		if (even_odd == EVEN)
			UCA0CTL0 |= UCPAR;		// Set even parity
		if (stop_bit == ONE_STOP)
			UCA0CTL0 &= ~UCSPB;		// Set one stop bit
		if (stop_bit == TWO_STOP)
			UCA0CTL0 |= UCSPB;		// Set two stop bits
	}

	if (mode == AUTOBAUD){
	};

	// Pin configuration
	P3SEL |= BIT3;
	P3DIR |= BIT3;					// TXD = OUT = 1
	P3SEL |= BIT4;
	P3DIR &= ~BIT4;					// RXD = IN = 0


	UCA0CTL1 &= ~UCSWRST;			// Disable software reset of USCI module
}



// Function initiliazes baudrate speed for UART communication
// Frequency BRCLK is set to 4MHz also necessary for calculations
// Low frequency mode is used UCOS16 = 0
//!!!!!!!!!!!!! By changing BRCLK frequency is necessary to recalculate values
// of UCBRx and UCBRSx at low frequency mode !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!! For this you can use UART low freq baudrate calc.xls file !!!!!!!!!!!!
// If you do not have this file you have to use equations from family datasheet of MCU
void UART_baudrate(uint32_t baudrate)
{
	//local variables

	UCA0CTL1 |= UCSWRST;			// Enable software reset of USCI module
	UCA0MCTL &= ~UCOS16;			// Switch off oversampling mode for high frequencies BRCLK

	switch (baudrate) {

		case 110 :
			UCA0BRW = 36363;				// Baudrate 110
			UCA0MCTL |= UCBRS2 + UCBRS0;	// UCBRSx = 5
			break;
		case 150 :
			UCA0BRW = 26666;				// Baudrate 150
			UCA0MCTL |= UCBRS2 + UCBRS0;	// UCBRSx = 5
			break;
		case 300 :
			UCA0BRW = 13333;				// Baudrate 300
			UCA0MCTL |= UCBRS1;				// UCBRSx = 2
			break;
		case 1200 :
			UCA0BRW = 3333;					// Baudrate 1200
			UCA0MCTL |= UCBRS1;				// UCBRSx = 2
			break;
		case 2400 :
			UCA0BRW = 1666;					// Baudrate 2400
			UCA0MCTL |= UCBRS2 + UCBRS0;	// UCBRSx = 5
			break;
		case 4800 :
			UCA0BRW = 833;					// Baudrate 4800
			UCA0MCTL = UCBRS1;				// UCBRSx = 2
			break;
		case 9600 :
			UCA0BRW = 416;					// Baudrate 9600
			UCA0MCTL |= UCBRS2 + UCBRS0;	// UCBRSx = 5
			break;
		case 19200 :
			UCA0BRW = 208;					// Baudrate 19200
			UCA0MCTL |= UCBRS1;				// UCBRSx = 2
			break;
		case 38400 :
			UCA0BRW = 104;					// Baudrate 38400
			UCA0MCTL |= UCBRS0;				// UCBRSx = 1
			break;
		case 57600 :
			UCA0BRW = UCBRS1 + UCBRS0;		// Baudrate 57600
			UCA0MCTL = 3;					// UCBRSx = 3
			break;
		case 115200 :
			UCA0BRW = 34;					// Baudrate 1200
			UCA0MCTL |= UCBRS2 + UCBRS0;	// UCBRSx = 5
			break;
		case 230400 :
			UCA0BRW = 17;					// Baudrate 230400
			UCA0MCTL |= UCBRS1;				// UCBRSx = 2
			break;
		case 460800 :
			UCA0BRW = 8;					// Baudrate 460800
			UCA0MCTL |= UCBRS2 + UCBRS0;	// UCBRSx = 5
			break;
		case 921600 :
			UCA0BRW = 4;					// Baudrate 921600
			UCA0MCTL |= UCBRS1;				// UCBRSx = 2
			break;
	}

	UCA0CTL1 &= ~UCSWRST;			// Disable software reset of USCI module
}


inline void UART_txd(uint8_t data)
{
	while (!(UCA0IFG & UCTXIFG));		// While transmitt buffer not empty wait
		UCA0TXBUF = data;				// Transmit buffer empty= 1 load data to
}

uint8_t UART_rxd(uint8_t *data)
{
	// local var

	while(!UCA0IFG & UCRXIFG);			// While receive buffer empty wait

	// When receive buffer received character
	if (UCA0STAT & UCOE)				// If overflow flag set write to variable
		return(UCA0STAT & UCOE);

		*data = UCA0RXBUF;				// Read character from receive buffer

	if (UCA0STAT & UCOE)
		return(UCA0STAT & UCOE);

	return(UCA0STAT & UCOE);
}

