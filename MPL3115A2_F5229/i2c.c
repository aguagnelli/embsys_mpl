/*
 * i2c.c
 *
 *  Created on: 14/dic/2014
 *      Author: Alessandro
 */
#include "i2c.h"

unsigned int isRead;				// Flag to understand if the I2C operation is a read or a write (since we need to send a restart condition if we are writing)
unsigned int TXByteCtr; 			// Number of byte to send
unsigned char TXDataBuffer[128];	// Buffed for data to transmit
unsigned char RXData;				// Buffer for received data

// Initialize I2C bus for write
void i2c_writeInit(){
	  UCB0CTL1 |= UCTR;                         	// UCTR=1 => Transmit Mode (R/W bit = 0)
	  UCB0IFG &= ~UCTXIFG;							// Remove previous TX interrupt
	  UCB0IE &= ~UCRXIE;                         	// disable Receive ready interrupt
	  UCB0IE |= UCTXIE;                          	// enable Transmit ready interrupt
}

// Initialize I2C bus for read
void i2c_readInit(){
	  UCB0CTL1 &= ~UCTR;                        // UCTR=0 => Receive Mode (R/W bit = 1)
	  UCB0IFG &= ~UCRXIFG;					// Remove previous RX interrupt
	  UCB0IE &= ~UCTXIE;                         // disable Transmit ready interrupt
	  UCB0IE |= UCRXIE;                          // enable Receive ready interrupt
}
// Initialize I2C
void InitI2C(unsigned char slave_address)
{
	P3SEL |= 0x03;        					// Assign I2C pins to USCI_B0
	P3REN |= 0x03;							// Enable Pull up resistor

	// Recommended initialisation steps of I2C module as shown in User Guide:
	UCB0CTL1 |= UCSWRST;					// Enable SW reset
	UCB0CTL0 = UCMST + UCMODE_3 + UCSYNC;   // I2C Master, synchronous mode
	UCB0CTL1 = UCSSEL_2 + UCTR + UCSWRST;   // Use SMCLK, TX mode, keep SW reset
	UCB0BR0 = 12;                           // fSCL = SMCLK/12 = ~100kHz
	UCB0BR1 = 0;
	UCB0I2CSA  = slave_address;          	// define Slave Address
	UCB0CTL1 &= ~UCSWRST;                   // Clear SW reset, resume operation
}

//------------------------------------------------------------------------------
// The USCIAB0_ISR is structured such that it can be used to transmit any
// number of bytes by pre-loading TXByteCtr with the byte count.
//------------------------------------------------------------------------------
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = USCI_B0_VECTOR
__interrupt void USCI_B0_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(USCI_B0_VECTOR))) USCI_B0_ISR (void)
#else
#error Compiler not supported!
#endif
{
  switch(__even_in_range(UCB0IV,12))
  {
  case  0: break;                           // Vector  0: No interrupts
  case  2: break;                           // Vector  2: ALIFG
  case  4: break;                           // Vector  4: NACKIFG
  case  6: break;                           // Vector  6: STTIFG
  case  8: break;                           // Vector  8: STPIFG
  case 10:
	  RXData = UCB0RXBUF;                  // store received data in buffer
	  __bic_SR_register_on_exit(LPM0_bits);   // Exit LPM0
	  break;                           // Vector 10: RXIFG
  case 12:                                  // Vector 12: TXIFG
    if (TXByteCtr)                          // Check TX byte counter
    {
      TXByteCtr--;                          // Decrement TX byte counter
      UCB0TXBUF = TXDataBuffer[TXByteCtr];                   // Load TX buffer
    }
    else if (!isRead)						// If it was a read we don't send the stop condition since we need a repeated start
    {
      UCB0CTL1 |= UCTXSTP;                  // I2C stop condition
      UCB0IFG &= ~UCTXIFG;                  // Clear USCI_B0 TX int flag
      __bic_SR_register_on_exit(LPM0_bits); // Exit LPM0
    }else if (isRead){
    	UCB0IFG &= ~UCTXIFG;                  // Clear USCI_B0 TX int flag
    	__bic_SR_register_on_exit(LPM0_bits); // Exit LPM0
    }
    break;
  default: break;
  }
}

// Writes a single byte to the slave on the specified register
void i2c_wByte(unsigned char reg, unsigned char byte){
	isRead = 0;
	while (UCB0STAT & UCBUSY);                	// wait until I2C module has
												// finished all operations
	TXDataBuffer[1] = reg;						// Prepare output array
	TXDataBuffer[0] = byte;
	TXByteCtr = 2;								// Sets number of bytes to write
	i2c_writeInit();							// Set module for write
	UCB0CTL1 |= UCTXSTT;                      	// start condition generation
												// => I2C communication is started
	__bis_SR_register(LPM0_bits + GIE);       	// Enter LPM0 w/ interrupts
	UCB0CTL1 |= UCTXSTP;                      	// I2C stop condition
	while(UCB0CTL1 & UCTXSTP);                	// Ensure stop condition got sent
}

// Reads a byte from the slave from the specified register
unsigned char i2c_rByte(unsigned char reg){
	isRead = 1;
	while (UCB0STAT & UCBUSY);                // wait until I2C module has
	                                            // finished all operations
	  TXDataBuffer[0] = reg;					// Set the register as control byte
	  TXByteCtr = 1;							// Set the number of bytes to write
	  i2c_writeInit();
	  UCB0CTL1 |= UCTXSTT;                      	// start condition generation
	  	  	  	  	  	  	  	  	  	  	  	  // => I2C communication is started
	  __bis_SR_register(LPM0_bits + GIE);       	// Enter LPM0 w/ interrupts

	  i2c_readInit();
	  UCB0CTL1 |= UCTXSTT;                      // I2C start condition
	  while(UCB0CTL1 & UCTXSTT);                // Start condition sent?
	  UCB0CTL1 |= UCTXSTP;                      // I2C stop condition
	  __bis_SR_register(LPM0_bits + GIE);       // Enter LPM0 w/ interrupts
	  while(UCB0CTL1 & UCTXSTP);                // Ensure stop condition got sent
	  return RXData;
}



