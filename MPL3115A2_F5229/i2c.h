/*
 * i2c.h
 *
 *  Created on: 14/dic/2014
 *      Author: Alessandro
 */

#ifndef I2C_H_
#define I2C_H_
#include <msp430.h>
#include <stdint.h>
// Initialize I2C bus
void InitI2C(unsigned char slave_address);
// Initialize I2C bus for write
void i2c_writeInit();
// Initialize I2C bus for read
void i2c_readInit();
// Writes a single byte to the slave on the specified register
void i2c_wByte(unsigned char reg, unsigned char byte);
// Reads a byte from the slave from the specified register
unsigned char i2c_rByte(unsigned char reg);
#endif /* I2C_H_ */
