#include <msp430.h>
#include <stdint.h>
#include "UART.h"
#include "LCD.h"
#include "i2c.h"

#define I2C_MLPADDRESS 0x60			// Address of the device
#define MPL_CTRL_REG1 0x26			// Address of Control Register 1
#define MPL_STATUS_REG 0x00			// Address of status register
#define MPL_ALT_128_A 0xB9			// Working mode: altimeter with 128 sample, active
#define MPL_BAR_128_A 0x39			// Working mode: barometer with 128 sample, active

#define MODE_ALTIMETER 2			// Define used to compare the working mode
#define MODE_BAROMETER 4			// Define used to compare the working mode

#define LEFT_BUTTON BIT1			// Location of left Button
#define RED_LED BIT0				// Location of red led
#define GREEN_LED BIT7				// Location of green led

unsigned int mode;					// Working mode (altimeter, barometer)

// Conversion variables
unsigned char status_reg, temp_MSB, temp_LSB, press_MSB, press_CSB, press_LSB;		// Variable to hold the raw data from the sensor
uint32_t temp_convert;
uint32_t press_convert;
int16_t integer_temp;
int32_t integer_press;
float float_temp;
float float_press;
unsigned int press_first_wrote;
int32_t integer_for_conversion; // Variable to temporarly mantain the value being converted
inline void setClock();

int main(void)
{

	WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT

	setClock();								// Set clock to 4MHz
	// Initalize output led
	P1DIR |= RED_LED;
	P4DIR |= GREEN_LED;
	// Initialize button for interrupt
	P2REN |= LEFT_BUTTON;                 // Enable internal pull-up/down resistors
	P2OUT |= LEFT_BUTTON;                 //Select pull-up mode for P1.3
	P2IE |= LEFT_BUTTON;                    // P1.3 interrupt enabled
	P2IES |= LEFT_BUTTON;                  // P1.3 Hi/lo edge
	P2IFG &= ~LEFT_BUTTON;               // P1.3 IFG cleared
	// Initialize timer
	/*TA0CCR0 = 32432;					// Count limit (16 bit), 1 second
	TA0CCTL0 = CCIE;					// Enable counter interrupts, bit 4=1
	TA0CTL = TASSEL_1 + MC_1; 			// Timer A 0 with ACLK @ 12KHz, count UP*/
	TA0CCR0 = 62500;							// Count limit (16 bit), 1 second
	TA0CCTL0 = CCIE;							// Enable counter interrupts, bit 4=1
	TA0CTL = TASSEL_1 + MC_1; 					// Timer A 0 with ACLK @ 4MHz, count UP
	TA0CTL |= ID__8;							// Timer A divided by 8
	TA0EX0 |= TAIDEX_7;							// Timer A extended division by 8

	// Initialize I2C and sensor
	InitI2C(I2C_MLPADDRESS);
	i2c_wByte(0x13, 0x07);						// Enables data flags
	i2c_wByte(MPL_CTRL_REG1, MPL_ALT_128_A);	// Set the working mode for the sensor (altimeter, 128 samples, active)


	// UART init
	UART_init(UART, EIGHT, NOPARITY, EVEN, ONE_STOP);
	UART_baudrate(9600);

	// Debug init
	P6DIR |= BIT4 + BIT3;
	P6OUT = 0;
	// Initialize state and go to sleep
	mode = MODE_ALTIMETER;
	P1OUT |= RED_LED;
	P4OUT &= ~GREEN_LED;

	while(1){
		_BIS_SR(LPM0_bits + GIE); // LPM0 (low power mode) with interrupts enabled
	}
}


// Port 1 (button) interrupt service routine
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{
   P2IFG &=~LEFT_BUTTON;                        // P2.1 IFG cleared
   TA0CCTL0 &= ~CCIE;							// Disable interrupts for the timer while we change mode
   if (mode == MODE_ALTIMETER){
	   mode = MODE_BAROMETER;
	   P1OUT &= ~RED_LED;
	   i2c_wByte(0x13, 0x07);						// Enables data flags
	   i2c_wByte(MPL_CTRL_REG1, MPL_BAR_128_A);	// Set the working mode for the sensor (barometer, 128 samples, active)
   }
   else{
	   mode = MODE_ALTIMETER;
	   P1OUT |= RED_LED;
	   i2c_wByte(0x13, 0x07);						// Enables data flags
	   i2c_wByte(MPL_CTRL_REG1, MPL_ALT_128_A);	// Set the working mode for the sensor (altimeter, 128 samples, active)
   }
   TA0CCTL0 |= CCIE;							// Enables timer interrupt again
}

// Timer interrupt vector
#pragma vector=TIMER0_A0_VECTOR
   __interrupt void Timer0_A0 (void) {		// Timer0 A0 interrupt service routine
	P6OUT |= BIT4;
	TA0CCTL0 &= ~CCIE; // Disable interrupt
	P4OUT ^= GREEN_LED;
	status_reg = i2c_rByte(MPL_STATUS_REG);
	if(status_reg & 0x08){					// If data is ready
		// Read data
		press_MSB = i2c_rByte(0x01);
		press_CSB = i2c_rByte(0x02);
		press_LSB = i2c_rByte(0x03);
		temp_MSB = i2c_rByte(0x04);
		temp_LSB = i2c_rByte(0x05);
		// Prepare data
		integer_temp = (temp_MSB << 8) | temp_LSB;
		float_temp = (float) integer_temp / 256;
		if (mode == MODE_ALTIMETER){
			integer_press = press_MSB;
			integer_press = integer_press << 8;
			integer_press |= press_CSB;
			integer_press = integer_press << 8;
			integer_press |= press_LSB;
			integer_press = integer_press << 8;
			float_press = (float) integer_press / 65536;
		}else{
			integer_press = press_MSB;
			integer_press = integer_press << 8;
			integer_press |= press_CSB;
			integer_press = integer_press << 8;
			integer_press |= press_LSB;
			float_press = (float) integer_press / 64;
		}
	}
	P6OUT |= BIT3;
	// Update LCD
	LCD_clear();
	LCD_pos(line101);
	LCD_wchar(T);
	LCD_wchar(e);
	LCD_wchar(m);
	LCD_wchar(p);
	LCD_wchar(colon);
	LCD_wchar(space);
	integer_for_conversion = float_temp * 1000; 						// integer_temp_for_conversion = 24125
	if (integer_for_conversion < 0){									// If temperature is lower then 0 writes the minus and transorms it in positive temperature
		integer_for_conversion = integer_for_conversion * (-1);
		LCD_wchar(minus);
	}
	temp_convert = integer_for_conversion / 10000;						// temp_convert = 24125 / 10000 = 2
	if (temp_convert > 0)													// Writes the first 0 only
		LCD_wchar(0x30 + temp_convert);
	integer_for_conversion %= 10000;									// integer_temp_for_conversion = 24125 % 10000 = 4125
	temp_convert = integer_for_conversion / 1000;						// temp_convert = 4125 / 1000 = 4
	LCD_wchar(0x30 + temp_convert);
	LCD_wchar(dot);
	integer_for_conversion %= 1000;									// (and so on)
	temp_convert = integer_for_conversion / 100;
	LCD_wchar(0x30 + temp_convert);
	/*integer_temp_for_conversion %= 100;
	integer_temp_for_conversion = integer_temp_for_conversion / 10;
	LCD_wchar(0x30 + temp_convert);
	temp_convert = integer_temp_for_conversion % 10;
	LCD_wchar(0x30 + temp_convert);*/ // These are the second and the third digits after the comma and one is enough
	LCD_wchar(space);
	LCD_wchar(degree);
	LCD_wchar(Ci);
	// Update pression
	LCD_pos(line201);
	if (mode == MODE_ALTIMETER){
		LCD_wchar(A);
		LCD_wchar(l);
		LCD_wchar(t);
		LCD_wchar(colon);
		LCD_wchar(space);
		integer_for_conversion = float_press * 10;
		if (integer_for_conversion < 0){									// If altitude is lower then 0 writes the minus and transorms it in positive altitude
			integer_for_conversion = integer_for_conversion * (-1);
			LCD_wchar(minus);
		}
		press_first_wrote = 0;
		press_convert = integer_for_conversion / 10000;
		if (press_convert > 0){												// Doesn't write if it's a zero
			LCD_wchar(0x30 + press_convert);
			press_first_wrote = 1;
		}
		integer_for_conversion %= 10000;
		press_convert = integer_for_conversion / 1000;
		if (press_convert > 0 || press_first_wrote){												// Doesn't write if it's a zero
			LCD_wchar(0x30 + press_convert);
			press_first_wrote = 1;
		}
		integer_for_conversion %= 1000;
		press_convert = integer_for_conversion / 100;
		if (press_convert > 0 || press_first_wrote){												// Doesn't write if it's a zero
			LCD_wchar(0x30 + press_convert);
			press_first_wrote = 1;
		}
		integer_for_conversion %= 100;
		press_convert = integer_for_conversion / 10;
		LCD_wchar(0x30 + press_convert);
		LCD_wchar(dot);
		press_convert = integer_for_conversion % 10;
		LCD_wchar(0x30 + press_convert);
		LCD_wchar(space);
		LCD_wchar(m);
	}
	else{
		LCD_wchar(P);
		LCD_wchar(r);
		LCD_wchar(e);
		LCD_wchar(s);
		LCD_wchar(colon);
		LCD_wchar(space);
		press_first_wrote = 0;
		integer_for_conversion = float_press;
		press_convert = integer_for_conversion / 100000;
		if (press_convert > 0){												// Doesn't write if it's a zero
			LCD_wchar(0x30 + press_convert);
			press_first_wrote = 1;
		}
		integer_for_conversion %= 100000;
		press_convert = integer_for_conversion / 10000;
		if (press_convert > 0 || press_first_wrote){												// Doesn't write if it's a zero
			LCD_wchar(0x30 + press_convert);
			press_first_wrote = 1;
		}
		integer_for_conversion %= 10000;
		press_convert = integer_for_conversion / 1000;
		if (press_convert > 0 || press_first_wrote){												// Doesn't write if it's a zero
			LCD_wchar(0x30 + press_convert);
			press_first_wrote = 1;
		}
		integer_for_conversion %= 1000;
		press_convert = integer_for_conversion / 100;
		if (press_convert > 0 || press_first_wrote){												// Doesn't write if it's a zero
			LCD_wchar(0x30 + press_convert);
			press_first_wrote = 1;
		}
		integer_for_conversion %= 100;
		press_convert = integer_for_conversion / 10;
		LCD_wchar(0x30 + press_convert);
		press_convert = integer_for_conversion % 10;
		LCD_wchar(0x30 + press_convert);
		LCD_wchar(space);
		LCD_wchar(k);
		LCD_wchar(P);
		LCD_wchar(a);
	}
	P6OUT &= ~BIT3;
	P6OUT &= ~BIT4;
	TA0CCTL0 |= CCIE; // Enable interrupt again
}

inline void setClock(){
	/*****************************************************************************
	************************* Clock configuration *******************************
	*****************************************************************************/

	//XT1 pin assignment
	P5SEL 	|= BIT4;
	P5DIR 	&= ~BIT4;
	P5SEL 	|= BIT5;
	P5DIR 	|= BIT5;

	UCSCTL3 |= SELREF_0;      							// Set DCO FLL reference = XT1CLK
	UCSCTL4 |= SELA_4;        							// Set ACLK = DCOCLKDIV

	__bis_SR_register(SCG0);     						// Disable the FLL control loop
	UCSCTL0 = 0x0000;            						// Set lowest possible DCOx, MODx

	UCSCTL1 = DCORSEL_3;       							// Select DCO range 6MHz operation

	UCSCTL2 = FLLD_1 + 121;                				// Set DCO Multiplier for 4MHz
														// (N + 1) * (FLLRef / n) = Fdcoclkdiv
														// (121 + 1) * 32768 = 4MHz
														// Set FLL Div = fDCOCLK/2

	__bic_SR_register(SCG0);               				// Enable the FLL control loop

	// Worst-case settling time for the DCO when the DCO range bits have been
	// changed is (n x 32 x 32)f_FLL_reference. See UCS chapter in 5xx
	// UG for optimization.
	// 32 x 32 = 1024 = MCLK cycles for DCO to settle

	__delay_cycles(1024);

	// Loop until XT1,XT2 & DCO fault flag is cleared

	do {
		UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);			// Clear XT2,XT1,DCO fault flags
		SFRIFG1 &= ~OFIFG;                      			// Clear fault flags
	} while (SFRIFG1&OFIFG);                   			// Test oscillator fault flag
}
