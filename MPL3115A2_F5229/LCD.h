/* File: LCD.h
 * Author: Martin D�da
 * Date: 19.11.2014
 * Brief: Header file for LCD 2x16 characters display
 */

#ifndef LCD_H_
#define LCD_H_
#include <msp430.h>
#include <stdint.h>
#include "UART.h"

/********************************* Definitions and macros ************************************/


/********************************* Global variables ******************************************/
typedef enum {
				clear_LCD = 0x01,
				move_right_one = 0x14,
				move_left_one = 0x10,
				scroll_right = 0x1C,
				scroll_left = 0x18,
				display_on = 0x0C,
				display_off = 0x08,
				underline_on = 0x0E,
				underline_off = 0x0C,
				blink_on = 0x0D,
				blink_off = 0x0C,
				cursor_position = 0x80
			 } commands;

// Position on 2x 16 character display is for 1.line 0-19
//			 	 	 	 	 	 	 	 	  2.line 64-83 + cursor_position

typedef enum { env_command = 0x7C, wr_command = 0xFE} special_commands;   //env_command = 124 dec, wr_commend = 254 dec

typedef enum { line101 = 0x80, line102 = 0x81, line103 = 0x82, line104 = 0x83, line105 = 0x84, line106 = 0x85, line107 = 0x86,
			   line108 = 0x87, line109 = 0x88, line110 = 0x89, line111 = 0x8A, line112 = 0x8B, line113 = 0x8C, line114 = 0x8D,
			   line115 = 0x8E, line116 = 0x8F, line201 = 0xC0, line202 = 0xC1, line203 = 0xC2, line204 = 0xC3, line205 = 0xC4,
			   line206 = 0xC5, line207 = 0xC6, line208 = 0xC7, line209 = 0xC8, line210 = 0xC9, line211 = 0xCA, line212 = 0xCB,
			   line213 = 0xCC, line214 = 0xCD, line215 = 0xCE, line216 = 0xCF
			 } cursor_pos_tab;

typedef enum { A = 0x41, B = 0x42, Ci = 0x43, D = 0x44, E = 0x45, F = 0x46, G = 0x47, H = 0x48, I = 0x49, J = 0x4A, K = 0x4B,
			   L = 0x4C, M = 0x4D, eN = 0x4E, O = 0x4F, P = 0x50, Q = 0x51, R = 0x52, S = 0x53, T = 0x54, U = 0x55, Vi = 0x56,
			   W = 0x57, X = 0x58, Y = 0x59, Zet = 0x5A,
			   a = 0x61, b = 0x62, c = 0x63, d = 0x64, e = 0x65, f = 0x66, g = 0x67,
			   h = 0x68, i = 0x69, j = 0x6A, k = 0x6B, l = 0x6C, m = 0x6D, n = 0x6E, o = 0x6F, p = 0x70, q = 0x71, r = 0x72,
			   s = 0x73, t = 0x74, u = 0x75, v = 0x76, w = 0x77, x = 0x78, y = 0x79, z = 0x7A,
} alphabet;

typedef enum { line_feed = 0x0A, space = 0x20, exclam = 0x21, quote = 0x22, round_bracket_l = 0x28, round_bracket_r = 0x29, comma = 0x2C, minus = 0x2D, dot = 0x2E, zero = 0x30, one = 0x31, two = 0x32,
			   three = 0x33, four = 0x34, five = 0x35, six = 0x36, seven = 0x37, eight = 0x38, nine = 0x39, less_than = 0x3C,
			   equal = 0x3D, greater_than = 0x3E, question = 0x3F, colon = 0x3A, semicolon = 0x3B, bracket_l = 0x3C, bracket_r = 0x3E, degree = 0xDF
             } special_characters;

typedef enum { Off = 0x80, ten_percent = 0x83, twenty_percent = 0x86, thirty_percent = 0x89, forty_percent = 0x8C, fifty_percent = 0x8F,
			   sixty_percent = 0x92, seventy_percent = 0x95, eighty_percent = 0x98, ninty_percent = 0x9B, full_on = 0x9E
			 } backlight;


/******************************** Functions declaration **************************************/
inline void LCD_on();
inline void LCD_off();
void LCD_backlight(uint8_t light);
inline void LCD_clear();
inline void LCD_home();
inline void LCD_pos(uint8_t position);
inline void LCD_underline(uint8_t underline);
inline void LCD_wchar(uint8_t character);

#endif /* LCD_H_ */










