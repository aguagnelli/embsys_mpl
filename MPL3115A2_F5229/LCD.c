/* File: LCD.c
 * Author: Martin D�da
 * Date: 19.11.2014
 * Brief: Source file with functionality for LCD 2x16characters display
 */

/*********************************** Includes **************************************/
#include "LCD.h"
/****************************** Global variables ***********************************/



/***************************** Function definitions ********************************/
inline void LCD_on()
{
	UART_txd(wr_command);
	UART_txd(display_on);
}

inline void LCD_off()
{
	UART_txd(wr_command);
	UART_txd(display_off);
}
void LCD_backlight(uint8_t light)
{
	UART_txd(env_command);
	UART_txd(light);
}

inline void LCD_clear()
{
	UART_txd(wr_command);
	UART_txd(clear_LCD);
}

inline void LCD_pos(uint8_t position)
{
	UART_txd(wr_command);
	UART_txd(position);
}

inline void LCD_home()
{
	UART_txd(wr_command);
	UART_txd(cursor_position);
}

inline void LCD_underline(uint8_t underline)
{
	UART_txd(wr_command);
	UART_txd(underline);
}
inline void LCD_wchar(uint8_t character)
{
	UART_txd(character);
}
