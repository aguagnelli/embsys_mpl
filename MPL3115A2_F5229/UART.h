/* File: UART.h
 * Author: Martin Dida
 * Created on: 11.11.2014
 * Brief: Header file for UART functionality
 */

#ifndef UART_H_
#define UART_H_
#include <msp430.h>
#include <stdint.h>


/***************************** macros and definitions *******************************/


/**************************** global variables **************************************/
typedef enum {UART, IDLE_LINE, MULTIPROC, AUTOBAUD} UART_mode;
typedef enum {EIGHT, SEVEN} length;
typedef enum {NOPARITY, PARITY} parity_active;
typedef enum {ODD, EVEN} parity_sel;
typedef enum {ONE_STOP, TWO_STOP} stop_bit;
typedef enum {LOWFREQ, OVERSAMPLE} freq_mode;

// For Sparkfun LCD pin 3.3 on MSP board is used like TXD pin and pin 3.4 is used like RXD pin for UART


/**************************** function declarations ********************************/
void UART_init(uint8_t mode, uint8_t w_length, uint8_t parity, uint8_t even_odd, uint8_t stop_bit);
void UART_baudrate(uint32_t baudrate);
inline void UART_txd(uint8_t data);
uint8_t UART_rxd(uint8_t *data);

#endif /* UART_H_ */
